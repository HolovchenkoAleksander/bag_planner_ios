//
//  ThingTableViewCell.swift
//  Bag Planner
//
//  Created by Alexander on 11.09.2021.
//

import UIKit


class ThingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var toTakeButtonTapped: UIButton!
    
    @IBOutlet weak var imageViewForCell: UIImageView!
    
    @IBOutlet weak var thingNameLabel: UILabel!

    
    func configure(title: String) {
        self.thingNameLabel.text = title
        self.imageViewForCell.image = UIImage(named: title)
    }
}
