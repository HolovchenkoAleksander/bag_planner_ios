//
//  ViewController.swift
//  Bag Planner
//
//  Created by qa on 9/4/21.
//


import UIKit

class ThingsListFullViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var things: [Thing]? = []
    var realmMethods = RealmMethods()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Give a name to the title of the ViewController
//        self.title = Constans.ui.titleFullListViewController
        
        // Register cell
        let cellNib = UINib(nibName: "ThingTableViewCell", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: Constans.ui.defaultCellIdentifier)
        
        //Fill in the list with default data
        for i in Constans.listOfThingsFull.listOfThingsInTheMaternityHospitalFull {
            self.realmMethods.save(i)
        }
        
        
        // We get the data for display and add it to the property of our controller:
        self.things = self.realmMethods.getThings()
        
        self.tableView.reloadData()
    }
    
    // By clicking on the plus sign, the function is executed:
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        
        // Create an allert controller:
        let alert = UIAlertController(title: Constans.ui.alertAddButtonTitle,
                                      message: Constans.ui.alertAddButtonMessage,
                                      preferredStyle: .alert)
        
        // We create a Save button with a closure - it will be executed by clicking on this button:
        let saveAction = UIAlertAction(title: Constans.ui.saveActionTitle, style: .default) { action in
            
            // We check if there is a textfield in the allert controller and if there is text in it:
            guard let textField = alert.textFields?.first,
                  let thingToSave = textField.text else {
                return
            }
            // We call the function of saving the name of the thing:
            self.realmMethods.save(thingToSave)
            
            // We update the data in the things property, and reload the data in the table:
            self.things = self.realmMethods.getThings()
            self.tableView.reloadData()
        }
        
        // We create another button - Cancel, with which it will be possible to close it:
        let cancelAction = UIAlertAction(title: Constans.ui.cancelActionTitle, style: .cancel)
        
        // Add textfield:
        alert.addTextField()
        
        // Add the buttons created above to the allert controller:
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        // Showing the allert controller:
        present(alert, animated: true)
    }
    
    
}

extension ThingsListFullViewController: UITableViewDataSource, UITableViewDelegate {
    
    // The method in which we specify the number of cells (rows) in the table:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.things?.count ?? 0
    }
    
    // The method in which we create each cell:
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let thing = self.things?[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constans.ui.defaultCellIdentifier) as? ThingTableViewCell  {
            cell.configure(title: thing?.name ?? "")
//            cell.delegate = self
            cell.toTakeButtonTapped.tag = indexPath.row
            cell.toTakeButtonTapped.addTarget(self, action: #selector(toTakeButtonWasTapped(sender: )), for: .touchUpInside)
//            cell.imageViewForCell(thing)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    
    @objc
    func toTakeButtonWasTapped(sender: UIButton) {
        let rowIndex: Int = sender.tag
        let thing = self.things?[rowIndex]
                realmMethods.saveToTakeList(thing!.name)
                self.tableView.reloadData()
    }
    //add to "To Take" list to pressed on cell
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let thing = self.things?[indexPath.row]
//        realmMethods.saveToTakeList(thing!.name)
//        self.tableView.reloadData()
//    }
}
