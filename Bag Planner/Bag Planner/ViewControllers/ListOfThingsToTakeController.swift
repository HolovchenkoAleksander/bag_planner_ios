//
//  ListOfThingsToTakeControllerViewController.swift
//  bag-planner
//
//  Created by qa on 9/2/21.
//

import UIKit

class ListOfThingsToTakeController: UIViewController {
    
    @IBOutlet weak var toTakeListTableView: UITableView!
    
    var thingsToTake: [ThingToTake]? = []
    var realmMethods = RealmMethods()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Give a name to the title of the ViewController:
//        self.title = Constans.ui.titleToTakeViewController
        
        // Register cell:
        self.toTakeListTableView.register(UITableViewCell.self, forCellReuseIdentifier: Constans.ui.toTakeListCellIdentifier)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // We get the data for display and add it to the property of our controller:
        self.thingsToTake = realmMethods.getThingsToTake()
        
        //Reload our TableView:
        toTakeListTableView.reloadData()
    }
    
    
    
    
    ///MARK: add button
    @IBAction func plusButtonPressed(_ sender: Any) {
        
        // Create an allert controller:
        let alert = UIAlertController(title: Constans.ui.alertAddButtonTitle,
                                      message: Constans.ui.alertAddButtonMessage,
                                      preferredStyle: .alert)
        
        // We create a Save button with a closure - it will be executed by clicking on this button:
        let saveAction = UIAlertAction(title: Constans.ui.saveActionTitle, style: .default) { action in
            
            // We check if there is a textfield in the allert controller and if there is text in it:
            guard let textField = alert.textFields?.first,
                  let thingToSave = textField.text else {
                return
            }
            // We call the function of saving the name of the thing:
            self.realmMethods.save(thingToSave)
            
            // We update the data in the things property, and reload the data in the table:
            self.thingsToTake = self.realmMethods.getThingsToTake()
            self.toTakeListTableView.reloadData()
        }
        
        // We create another button - Cancel, with which it will be possible to close it:
        let cancelAction = UIAlertAction(title: Constans.ui.cancelActionTitle, style: .cancel)
        
        // Add textfield:
        alert.addTextField()
        
        // Add the buttons created above to the allert controller:
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        // Showing the allert controller:
        present(alert, animated: true)
    }
}


extension ListOfThingsToTakeController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return thingsToTake?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let thing = self.thingsToTake?[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constans.ui.toTakeListCellIdentifier)
        cell?.textLabel?.text = thing?.name
        return cell ?? UITableViewCell()
    }
}
