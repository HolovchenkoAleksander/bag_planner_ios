//
//  MapController.swift
//  bag-planner
//
//  Created by qa on 9/2/21.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapViewController: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Basic setup us map:
        self.setupMap()
        
        // Add annotations:
        self.addAnnotations()
    }
    private func setupMap() {
        // Настраиваем карту: начальная позиция, тип, отображение компаса, отображение масштаба
        let coordinate = CLLocation(latitude: 46.51218180182741, longitude: 30.753658726810073)
        self.mapViewController.centerToLocation(coordinate, regionRadius: 12000)
        self.mapViewController.mapType = .hybrid
        self.mapViewController.showsCompass = true
        self.mapViewController.showsScale = true
    }

    private func addAnnotations() {
        // Добавляем аннотации для примера
        let firstHospitalAnnotation = MKPointAnnotation()
        firstHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.45539877411686,
                                                       longitude: 30.74205475662564)
        firstHospitalAnnotation.title = Constans.ui.firstHospitalAnnotationTitle
        firstHospitalAnnotation.subtitle = Constans.ui.firstHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(firstHospitalAnnotation)
        //
        let secondHospitalAnnotation = MKPointAnnotation()
        secondHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.48187557745186,
                                                       longitude: 30.720947575744912)
        secondHospitalAnnotation.title = Constans.ui.secondHospitalAnnotationTitle
        secondHospitalAnnotation.subtitle = Constans.ui.secondHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(secondHospitalAnnotation)
        //
        let fourthHospitalAnnotation = MKPointAnnotation()
        fourthHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.51027411036499,
                                                       longitude: 30.722738753817218)
        fourthHospitalAnnotation.title = Constans.ui.fourthHospitalAnnotationTitle
        fourthHospitalAnnotation.subtitle = Constans.ui.fourthHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(fourthHospitalAnnotation)
        //
        let fifthHospitalAnnotation = MKPointAnnotation()
        fifthHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.440298346399445,
                                                       longitude: 30.749204038441853)
        fifthHospitalAnnotation.title = Constans.ui.fifthHospitalAnnotationTitle
        fifthHospitalAnnotation.subtitle = Constans.ui.fifthHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(fifthHospitalAnnotation)
        //
        let seventhHospitalAnnotation = MKPointAnnotation()
        seventhHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.43533756176704,
                                                       longitude: 30.718906522194665)
        seventhHospitalAnnotation.title = Constans.ui.seventhHospitalAnnotationTitle
        seventhHospitalAnnotation.subtitle = Constans.ui.seventhHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(seventhHospitalAnnotation)
        //
        let regionalHospitalAnnotation = MKPointAnnotation()
        regionalHospitalAnnotation.coordinate = CLLocationCoordinate2D(latitude: 46.58962417009193,
                                                       longitude: 30.792122185489497)
        regionalHospitalAnnotation.title = Constans.ui.regionalHospitalAnnotationTitle
        regionalHospitalAnnotation.subtitle = Constans.ui.regionalHospitalAnnotationSubtitle
        self.mapViewController.addAnnotation(regionalHospitalAnnotation)
    }
}







// Метод вызывается для каждой конкретной аннотации. Создаем MKAnnotationView

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier,
                                                                   for: annotation)
        return annotationView
    }
    
}

// Метод написанный в расширении центрирует карту к области:

private extension MKMapView {
    
    func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 1000) {
        
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
