//
//  listOfThingsInTheMaternityHospital.swift
//  bag-planner
//
//  Created by qa on 8/22/21.
//
import UIKit



struct Constans {
    struct listOfThingsFull{
        static var listOfThingsInTheMaternityHospitalFull: [String] = ["Паспорт",
                                                                       "Обменная карта",
                                                                       "Мобильный телефон",
                                                                       "Зарядное устройство",
                                                                       "Тапочки",
                                                                       "Наличные деньги",
                                                                       "Носки",
                                                                       "Нижнее белье",
                                                                       "Халат",
                                                                       "Рубашка(футболка)",
                                                                       "Постельное белье",
                                                                       "Полотенце",
                                                                       "Одноразовые полотенца бумажные",
                                                                       "Туалетные принадлежности",
                                                                       "Приборы для приема пищи",
                                                                       "Подгузник",
                                                                       "Одежда малышу",
                                                                       "Пеленки",
                                                                       "Влажные салфетки детские",
                                                                       "Влажные салфетки для мамы",
                                                                       "Одежда парадная маме",
                                                                       "Одежда парадная малышу"]
    }
    struct ui {
        static let defaultCellIdentifier = "ThingTableViewCell"
        static let toTakeListCellIdentifier = "toTakeListTableView"
        
        static let titleToTakeViewController = "Необходимо собрать"
        static let titleFullListViewController = "Полный список вещей в роддом"
        
        static let saveActionTitle = "Save"
        static let cancelActionTitle =  "Cancel"
        static let alertAddButtonTitle = "Full list with things"
        static let alertAddButtonMessage = "Add a new thing"
        
        static let firstHospitalAnnotationTitle = "Роддом №1"
        static let firstHospitalAnnotationSubtitle = "Роддом Приморского района г. Одессы"
        static let secondHospitalAnnotationTitle = "Роддом №2"
        static let secondHospitalAnnotationSubtitle = "Роддом Приморского района г. Одессы"
        static let fourthHospitalAnnotationTitle = "Роддом №4"
        static let fourthHospitalAnnotationSubtitle = "Роддом Суворовского района г. Одессы"
        static let fifthHospitalAnnotationTitle = "Роддом №5"
        static let fifthHospitalAnnotationSubtitle = "Роддом Приморского района г. Одессы"
        static let seventhHospitalAnnotationTitle = "Роддом №7"
        static let seventhHospitalAnnotationSubtitle = "Роддом Малиновского района г. Одессы"
        static let regionalHospitalAnnotationTitle = "Областной Роддом"
        static let regionalHospitalAnnotationSubtitle = "Роддом Суворовского района г. Одессы"
        
    }
}
