//
//  RealmModel.swift
//  bag-planner
//
//  Created by qa on 8/28/21.
//
import RealmSwift

// Создаем объект реалм
let realm = try? Realm()

class Thing: Object {
    @objc dynamic var name = ""
    @objc dynamic var thingDescription: String = ""
    @objc dynamic var imageName: String = ""
}

class ThingToTake: Object {
    @objc dynamic var name = ""
}

class RealmMethods {
    
    // Добавляем функцию сохранения вещи в UserDefaults:

    func save(_ thingName: String) {

        // Get existing values
        let exisdedValues = self.getThings()
        let exisdedValuesWithThingName = exisdedValues.filter({ $0.name == thingName }).count
        
        if exisdedValuesWithThingName != 0 {
        } else {
            let thing = Thing()
            thing.name = thingName
            try? realm?.write {
                realm?.add(thing)
            }
        }
    }
    
    func saveToTakeList(_ thingNameToTake: String) {
        
        let exisdedValues = self.getThingsToTake()
        let exisdedValuesWithThingName = exisdedValues.filter({ $0.name == thingNameToTake }).count
        
        if exisdedValuesWithThingName != 0 {
        } else {
            let thingToTake = ThingToTake()
            thingToTake.name = thingNameToTake
            
            try? realm?.write {
                realm?.add(thingToTake)
            }
        }
    }

    // Добавляем функцию, которая достает данные из Realm и возвращает в виде массива:

    func getThings() -> [Thing] {

        var things = [Thing]()
        guard let thingsResults = realm?.objects(Thing.self) else { return [] }
        for thing in thingsResults {
            things.append(thing)
        }
        return things
    }
    
    
    public func getThingsToTake() -> [ThingToTake] {

        var thingsToTake = [ThingToTake]()
        guard let thingsToTakeResults = realm?.objects(ThingToTake.self) else { return [] }
        for thingToTake in thingsToTakeResults {
            thingsToTake.append(thingToTake)
        }
        return thingsToTake
    }
}
